Archstrap
=========
Create archlinux rootfs without pacman

.. code-block:: shell

	Usage:
	  archstrap [rootfs] (options)
	  -r: Use custom repository
	  -i: Disable section
	  -p: Use custom pacman.conf

Dependencies:
^^^^^^^^^^^^^
* busybox
* sed
* grep
* tar
* zstd


Examples:
^^^^^^^^^
.. code-block:: shell

	# create archlinux rootfs (no need any parameters)
	archstrap airootfs
	# create hyperbola-linux rootfs (community section not available for hyperbola-linux)
	archstrap hyperbola -i community -r 'https://mirror.fsf.org/hyperbola/gnu-plus-linux-libre/testing/$repo/os/$arch'
	# create manjaro rootfs (need only repo changes)
	archstrap manjaro -r ''https://mirrors.fossho.st/manjaro/stable/$repo/$arch'

Bugs
^^^^
* Gpg check is not supported.
* Disk space check broken.

Projects uses this
^^^^^^^^^^^^^^^^^^
* https://gitlab.com/tearch-linux/applications-and-tools/teaiso
* https://gitlab.com/sulincix/debian-subsystem
